
This is a basic login page implemented using NodeJS.
This will do the basic authentication.
Version 1.0

Through terminal, you will have to go to this repo's directory.
1. npm install // this will download all the dependancies except nedb(used for database)
2. node main.js // this will start the server in the url: [http://localhost:3000/](Link URL)
3. go to your webbrowser and type the same url given above
4. You are good to go!!!!!!

For any doubts contact via mail: [venkatss10@gmail.com](Link URL)
